import machine, sdcard, uos, onewire, ds18x20, time


## Temperature
# Set GPIO 20 as data pin for temp sensors
ds_pin = machine.Pin(20)
ds_sensor = ds18x20.DS18X20(onewire.OneWire(ds_pin))

# Each temp sensor has unique rom (ID)
roms = {'hot':b'(d+\x8b\r\x00\x00\xd4', 'cool':b'(\x1a\xaa\x8a\r\x00\x00\xd6', 'ambi':b'(\x06\xd3\x8a\r\x00\x00n'}

## SD Card
# Assign chip select (CS) pin (and start it high)
cs = machine.Pin(13, machine.Pin.OUT)

# Intialize SPI peripheral (start with 1 MHz)
spi = machine.SPI(1,
                  baudrate=1000000,
                  polarity=0,
                  phase=0,
                  bits=8,
                  firstbit=machine.SPI.MSB,
                  sck=machine.Pin(10),
                  mosi=machine.Pin(11),
                  miso=machine.Pin(12))

# Initialize SD card
sd = sdcard.SDCard(spi, cs)

# Mount filesystem
vfs = uos.VfsFat(sd)
uos.mount(vfs, "/sd")

with open("/sd/test01.txt", "w") as file:
    file.write("Hello, SD World!\r\n")

## Mosfet
# Mosfet Pin at GPIO 16. Be aware that a value of False will activate the peltier.
mosfet = machine.Pin(16, machine.Pin.OUT)
mosfet.value(False)

# Endless loop for measuring temperature and print results
loop = 0

while True:
    loop += 1
    ds_sensor.convert_temp()
    time.sleep_ms(1000)

    print('Temp cool: ' + str(ds_sensor.read_temp(roms['cool'])))
    print('Temp hot: ' + str(ds_sensor.read_temp(roms['hot'])))
    print('Temp ambi: ' + str(ds_sensor.read_temp(roms['ambi'])) + '\n')
    
    with open("/sd/temperatures.txt", "a") as file:
        file.write(str(loop) + ',' + str(ds_sensor.read_temp(roms['cool'])) + ',' + str(ds_sensor.read_temp(roms['hot'])) + ',' + str(ds_sensor.read_temp(roms['ambi'])) + '\r\n')
